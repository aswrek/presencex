# PresenceX

PresenceX is just simple RichPresence client for Discord.
Created on Clickteam Fusion 2.5 with DRPC extension (by Uppernate)

# Usage
First thing first, you need **Microsoft Windows** (that can run discord).
PresenceX don't works on linux (you can execute it with WINE, but it will do nothing)

In the textboxes, enter informations type what is written below the textbox.
Also, you need **application ID**. To get it, go on [discord dev portal](https://discord.com/developers/applications), and create new
application. Then, you will see Application ID.
To see how will be looks your custom status, go to https://discord.com/developers/applications/<your application id>/rich-presence/visualizer 

# The name
"PresenceX" consists of "presence" because the client shows presence on discord.
The "x" is so the name doesn't look weird.
